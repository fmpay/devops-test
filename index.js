var os = require('os')
var http = require('http')

function handleRequest(req, res) {
  res.write('Hi there!')
  res.end()
}

http.createServer(handleRequest).listen(3001)